import pygame
import matplotlib.path as mplpath
import numpy.core._methods, numpy.lib.format
from random import randint,choice
import math
import json

pygame.mixer.pre_init(44100, -16, 1, 512)
pygame.init()
screen=pygame.display.set_mode([1000,400])

pygame.display.set_caption("PHOTON CORPS")
icon = pygame.image.load("Display/photon.png")
pygame.display.set_icon(icon)
clock = pygame.time.Clock()

sfx_s_explode = pygame.mixer.Sound("Sounds/s_explode.wav")
sfx_lazer = pygame.mixer.Sound("Sounds/lazer.aiff")
sfx_lazer.set_volume(0.3)
sfx_lazer_fail = pygame.mixer.Sound("Sounds/lazer_fail.aiff")
sfx_lazer_fail.set_volume(0.2)
sfx_a_explode = pygame.mixer.Sound("Sounds/a_explode.aiff")
sfx_a_explode.set_volume(0.7)

pygame.mixer.music.load('Sounds/main_loop.wav')
pygame.mixer.music.set_volume(0.5)
pygame.mixer.music.play(-1)

class Game:
    def __init__(self):
        self.up_key_pressed = False
        self.down_key_pressed = False
        self.left_key_pressed = False
        self.right_key_pressed = False
        self.ship = Spaceship()
        self.rocks = Asteroids()
        with open('Data/text.json') as f:
            self.text = json.load(f)
        with open('Data/scores.json') as f:
            self.hi_scores = json.load(f)
        self.game = False
        self.score = 0
        self.score_board = False

    def draw_word(self, word,size=5,x=5,y=5):
        word = [c.lower() for c in word]
        x_i = x
        y_i = y
        for item in word:
            for x,y in self.text[item]:
                pygame.draw.rect(screen,[255,255,255],[[x_i+(x*size),y_i+(y*size)],[size,size]])
            x_i += 6*size

    def menu(self):
        self.draw_word("PHOTON CORPS",10,130,80)
        self.draw_word("press spacebar to play",2,340,250)
        self.draw_word("press s to see scores",2,345,300)

    def draw_scores(self):
        self.draw_word("hi scores:",3,400,10)
        x = 300
        y = 70
        for item in range(1,11):
            score = self.hi_scores[str(item)]["score"]
            name = self.hi_scores[str(item)]["name"]
            self.draw_word("{:>2}: {:>7} {:<5}".format(item,score,name),2,x,y)
            y += 30

    def draw_score(self):
        self.draw_word("score: {}".format(self.score),2,2,2)
        self.draw_word("hi score: {}".format(self.hi_scores["1"]["score"]),2,800,2)

    def draw_power(self):
        power = self.ship.power
        self.draw_word('power:',2,170,2)
        pygame.draw.rect(screen,[255,255,255],[[250,3],[450,20]])
        pygame.draw.rect(screen,[0,0,0],[[252,5],[446,16]])
        pygame.draw.rect(screen,[255,255,255],[[254,7],[442*(power/100),12]])

    def update_scores(self,game_score):
        while True:
            for item in range(1,11):
                if game_score > self.hi_scores[str(item)]["score"]:
                    next_s = self.hi_scores[str(item)]["score"]
                    next_n = self.hi_scores[str(item)]["name"]
                    self.hi_scores[str(item)]["score"] = game_score
                    name = ""
                    enter = False
                    while not enter:
                        for event in pygame.event.get():
                            if event.type == pygame.KEYDOWN: # If user wants to perform an action
                                if event.key == pygame.K_a:
                                    name += "a"
                                if event.key == pygame.K_b:
                                    name += "b"
                                if event.key == pygame.K_c:
                                    name += "c"
                                if event.key == pygame.K_d:
                                    name += "d"
                                if event.key == pygame.K_e:
                                    name += "e"
                                if event.key == pygame.K_f:
                                    name += "f"
                                if event.key == pygame.K_g:
                                    name += "g"
                                if event.key == pygame.K_h:
                                    name += "h"
                                if event.key == pygame.K_i:
                                    name += "i"
                                if event.key == pygame.K_j:
                                    name += "j"
                                if event.key == pygame.K_k:
                                    name += "k"
                                if event.key == pygame.K_l:
                                    name += "l"
                                if event.key == pygame.K_m:
                                    name += "m"
                                if event.key == pygame.K_n:
                                    name += "n"
                                if event.key == pygame.K_o:
                                    name += "o"
                                if event.key == pygame.K_p:
                                    name += "p"
                                if event.key == pygame.K_q:
                                    name += "q"
                                if event.key == pygame.K_r:
                                    name += "r"
                                if event.key == pygame.K_s:
                                    name += "s"
                                if event.key == pygame.K_t:
                                    name += "t"
                                if event.key == pygame.K_u:
                                    name += "u"
                                if event.key == pygame.K_v:
                                    name += "v"
                                if event.key == pygame.K_w:
                                    name += "w"
                                if event.key == pygame.K_x:
                                    name += "x"
                                if event.key == pygame.K_y:
                                    name += "y"
                                if event.key == pygame.K_z:
                                    name += "z"
                                if event.key == pygame.K_BACKSPACE:
                                    name = name[:-1]
                                if event.key == pygame.K_RETURN:
                                    enter = True
                                if len(name) > 5:
                                    name = name[1:]
                        pygame.draw.rect(screen,[0,0,0],[[0,0],[1000,400]])
                        self.draw_word("name: "+name,5,400,200)
                        pygame.display.flip()
                    self.hi_scores[str(item)]["name"] = name
                    for x in range(item+1,10):
                        old_s = self.hi_scores[str(x)]["score"]
                        self.hi_scores[str(x)]["score"] = next_s
                        old_n = self.hi_scores[str(x)]["name"]
                        self.hi_scores[str(x)]["name"] = next_n
                        next_s,next_n = old_s,old_n
                    break
            break
        with open('Data/scores.json', 'w') as f:
            json.dump(self.hi_scores,f)


    def collision(self):
        ship_body = mplpath.Path(self.ship.vertices())
        for rock in self.rocks.asteroids:
            if not rock.explode:
                if ship_body.contains_point([rock.a_coord[0],rock.a_coord[1]]):
                    self.ship.exploder()
                    rock.exploder()
                    self.update_scores(self.score)
                for point in self.ship.vertices():
                    if math.sqrt((point[0] - rock.a_coord[0])**2 + (point[1] - rock.a_coord[1])**2) < rock.a_size+1:
                        self.ship.exploder()
                        rock.exploder()
                        self.update_scores(self.score)
                for lazer in self.ship.lazers:
                    if math.sqrt((lazer.front[0]//10 - rock.a_coord[0])**2 + (lazer.front[1]//10 - rock.a_coord[1])**2) < rock.a_size+2:
                        rock.exploder()
                        self.score += int((20/rock.a_size)*10)*(self.rocks.speed+1)
                        self.ship.lazers.remove(lazer)

    def main_loop(self):
        running = True
        while running:
            if len(self.rocks.asteroids) == 0:
                for count in range(randint(5,15)):
                    self.rocks.asteroids.append(Asteroid())
                self.rocks.randomize()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                else:
                    if event.type == pygame.KEYDOWN: # If user wants to perform an action
                        if event.key == pygame.K_DOWN:
                            self.down_key_pressed = True
                        if event.key == pygame.K_UP:
                            self.up_key_pressed = True
                        if event.key == pygame.K_LEFT:
                            self.left_key_pressed = True
                        if event.key == pygame.K_RIGHT:
                            self.right_key_pressed = True
                        if event.key == pygame.K_SPACE and not self.game:
                            self.game = True
                        if event.key == pygame.K_s and not self.game:
                            self.score_board = True
                        if event.key == pygame.K_SPACE and self.game:
                            self.ship.shoot()
                        if event.key == pygame.K_a:
                            self.rocks.randomize()
                        if event.key == pygame.K_ESCAPE:
                            self.game = False
                            self.__init__()
                    if event.type == pygame.KEYUP: # If user wants to perform an action
                        if event.key == pygame.K_DOWN:
                            self.down_key_pressed = False
                        if event.key == pygame.K_UP:
                            self.up_key_pressed = False
                        if event.key == pygame.K_LEFT:
                            self.left_key_pressed = False
                        if event.key == pygame.K_RIGHT:
                            self.right_key_pressed = False

            clock.tick(30)
            if self.left_key_pressed:
                self.ship.turn_left()
            if self.right_key_pressed:
                self.ship.turn_right()
            if self.up_key_pressed:
                self.ship.speed_up()
            if self.down_key_pressed:
                self.ship.slow_down()
            if self.ship.speed != 0:
                self.ship.move_ship()
            pygame.draw.rect(screen,[0,0,0],[[0,0],[1000,400]])
            if self.game:
                self.collision()
                self.rocks.move_rocks()
                self.ship.draw()
                self.ship.power += 0.5 if self.ship.power < 100 else 0
                self.rocks.draw()
                self.draw_score()
                self.draw_power()
            if not self.game and self.score_board:
                self.draw_scores()
            if not self.game and not self.score_board:
                self.menu()
            pygame.display.flip()

        pygame.quit()


class Spaceship:
    def __init__(self):
        self.heading = 0
        self.speed = 0
        self.centre = [1100,1000]
        self.nose = [self.centre[0]+200,self.centre[1]]
        self.l_wing = [self.centre[0]-100,self.centre[1]-100]
        self.r_wing = [self.centre[0]-100,self.centre[1]+100]
        self.lazers = []
        self.power = 100
        self.explode = False
        self.particles = []

    def speed_up(self):
        if self.speed < 40:
            self.speed += 1

    def slow_down(self):
        if self.speed > -10:
            self.speed -= 1

    def rotate(self,anchor,point,angle):
        ox, oy = anchor
        px, py = point

        qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
        qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
        return [qx, qy]

    def move_ship(self):
        heading = self.heading
        speed = self.speed
        self.centre[0] = self.centre[0]+(math.cos(heading) * speed)
        self.centre[1] = self.centre[1]+(math.sin(heading) * speed)
        self.nose[0] = self.nose[0]+(math.cos(heading) * speed)
        self.nose[1] = self.nose[1]+(math.sin(heading) * speed)
        self.l_wing[0] = self.l_wing[0]+(math.cos(heading) * speed)
        self.l_wing[1] = self.l_wing[1]+(math.sin(heading) * speed)
        self.r_wing[0] = self.r_wing[0]+(math.cos(heading) * speed)
        self.r_wing[1] = self.r_wing[1]+(math.sin(heading) * speed)

    def turn_left(self):
        self.heading = self.heading - 0.10472
        centre = self.centre
        nose = self.nose
        l_wing = self.l_wing
        r_wing = self.r_wing
        heading = - 0.10472
        self.nose = self.rotate(centre,nose,heading)
        self.l_wing = self.rotate(centre,l_wing,heading)
        self.r_wing = self.rotate(centre,r_wing,heading)

    def turn_right(self):
        self.heading = self.heading + 0.10472
        centre = self.centre
        nose = self.nose
        l_wing = self.l_wing
        r_wing = self.r_wing
        heading = 0.10472
        self.nose = self.rotate(centre,nose,heading)
        self.l_wing = self.rotate(centre,l_wing,heading)
        self.r_wing = self.rotate(centre,r_wing,heading)

    def vertices(self):
        nose = [self.nose[0]//10,self.nose[1]//10]
        l_wing = [self.l_wing[0]//10,self.l_wing[1]//10]
        r_wing = [self.r_wing[0]//10,self.r_wing[1]//10]
        return [nose,l_wing,r_wing]

    def shoot(self):
        if self.power >= 20:
            nose = self.nose
            heading = self.heading
            sfx_lazer.play()
            self.lazers.append(Lazer(nose,heading))
            self.power -= 10
            if self.power < 0:
                self.power = 0
        else:
            sfx_lazer_fail.play()

    def exploder(self):
        if not self.explode:
            sfx_s_explode.play()
            coord = [self.centre[0]//10,self.centre[1]//10]
            size = 50
            for particle in range(size**2):
                self.particles.append(Particle(coord))
            self.explode = True

    def draw(self):
        if not self.explode:
            pygame.draw.polygon(screen,[255,255,255],self.vertices())
        if self.explode:
            self.centre = [-400,-400]
            self.nose = [-400,-400]
            self.l_wing = [-400,-400]
            self.r_wing = [-400,400]
            self.speed = 0
            for particle in self.particles:
                particle.move()
                p_xy = particle.verticies()
                p_x,p_y = p_xy[0],p_xy[1]
                pygame.draw.ellipse(screen,[255,255,255],[p_x,p_y,3,3])
                if particle.x - particle.dx > randint(30,80):
                    self.particles.remove(particle)
        for lazer in self.lazers:
            if -10 > lazer.vertices()[0][0] > 1010 or -10 > lazer.vertices()[0][1] > 410:
                self.lazers.remove(lazer)
            else:
                lazer.draw()
                lazer.move_lazer()

class Lazer:
    def __init__(self,coord,heading):
        self.l_speed = 100
        self.l_heading = heading
        self.length = 5
        self.front = [coord[0],coord[1]]
        self.back = [self.front[0]-(math.cos(heading) * self.l_speed), self.front[1]-(math.sin(heading) * self.l_speed)]

    def move_lazer(self):
        heading = self.l_heading
        speed = self.l_speed
        self.front[0] = (self.front[0]+((math.cos(heading) * speed)))
        self.front[1] = (self.front[1]+((math.sin(heading) * speed)))
        self.back[0] = (self.back[0]+((math.cos(heading) * speed)))
        self.back[1] = (self.back[1]+((math.sin(heading) * speed)))

    def vertices(self):
        front = [self.front[0]//10,self.front[1]//10]
        back = [self.back[0]//10,self.back[1]//10]
        return [front,back]

    def draw(self):
        pygame.draw.line(screen,[255,255,255],self.vertices()[0],self.vertices()[1],2)


class Asteroids:
    def __init__(self):
        self.count = randint(5,15)
        # self.count = 1
        self.asteroids = []
        self.speed = 0
        self.heading = 0
        for count in range(self.count):
            self.asteroids.append(Asteroid())

    def draw(self):
        for asteroid in self.asteroids:
            if asteroid.explode and len(asteroid.particles) == 0:
                self.asteroids.remove(asteroid)
            else:
                asteroid.draw()
    def move_rocks(self):
        heading = self.heading
        speed = self.speed
        for rock in self.asteroids:
            if not rock.explode:
                rock.a_coord[0] = (rock.a_coord[0]+((math.cos(heading) * speed))) % 1000
                rock.a_coord[1] = (rock.a_coord[1]+((math.cos(heading) * speed))) % 400

    def randomize(self):
        self.heading = randint(0,628)//100
        self.speed += randint(1,2 )


class Asteroid:
    def __init__(self):
        self.a_size = randint(5,15)
        self.a_coord = [randint(50,950),randint(50,350)]
        self.explode = False
        self.particles = []

    def draw(self):
        x,y = int(self.a_coord[0]),int(self.a_coord[1])
        size = int(self.a_size)
        if not self.explode:
            pygame.draw.circle(screen,[255,255,255],[x,y],size)
        if self.explode:
            for particle in self.particles:
                particle.move()
                p_xy = particle.verticies()
                p_x,p_y = p_xy[0],p_xy[1]
                pygame.draw.ellipse(screen,[255,255,255],[p_x,p_y,1,1])
                if particle.x - particle.dx > randint(30,80):
                    self.particles.remove(particle)

    def exploder(self):
        if not self.explode:
            sfx_a_explode.play()
            a_coord = self.a_coord
            size = self.a_size
            for particle in range(size**2):
                self.particles.append(Particle(a_coord))
            self.explode = True


class Particle:
    def __init__(self, coord):
        self.x = coord[0]
        self.y = coord[1]
        self.rx = randint(int(self.x-10),int(self.x+10))
        self.ry = randint(int(self.y-10),int(self.y+10))
        if self.x - self.rx != 0:
            self.dx = self.x - self.rx
        else:
            self.dx = choice([-1,1])
        if self.y - self.ry != 0:
            self.dy = self.y - self.ry
        else:
            self.dy = choice([-1,1])
        self.heading = math.cos(self.dy/self.dx)
        self.speed = randint(50,100)

    def move(self):
        heading = self.heading
        speed = self.speed
        self.rx = self.rx+(math.cos(heading) * speed)
        self.ry = self.ry+(math.cos(heading) * speed)

    def verticies(self):
        return [int(self.rx),int(self.ry)]


G = Game()
G.main_loop()

