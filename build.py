from shutil import copy,rmtree,copytree
from os import system,path,listdir,mkdir

print('Building: Photonz\n')

if path.exists('Photonz'):
    print('Removing previous build...\n')
    rmtree('Photonz')
print('running cxfreeze game.py -OO --target-dir=Photonz\Resources')
system('build.bat')

copy('photonz.bat','Photonz\photonz.bat')
for directory in listdir('Resources'):
    if not path.exists('Photonz\Resources\{}'.format(directory)):
        print('\nmaking directory Photonz\Resources\{}'.format(directory))
        mkdir('Photonz\Resources\{}'.format(directory))
    for file in listdir('Resources\{}'.format(directory)):
        print('copying file Resources\{0}\{1} to Photonz\Resources\{0}\{1}'.format(directory,file))
        copy('Resources\{}\{}'.format(directory,file),'Photonz\Resources\{}\{}'.format(directory,file))

print('\nBUILD SUCCESSFUL!')
