from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = [], excludes = [], includes = ['numpy.core._methods', 'numpy.lib.format'])

import sys
from shutil import copyfile,rmtree
base = 'Win32GUI' if sys.platform=='win32' else None


executables = [
    Executable('game.py', base=base, targetName = 'photonz')
]

rmtree('Photonz')

setup(name='Photon Corps',
      version = '0.1',
      description = 'A 2D space game',
      options = dict(build_exe = buildOptions),
      executables = executables)


